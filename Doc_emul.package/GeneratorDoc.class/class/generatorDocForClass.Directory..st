writing
generatorDocForClass: aClass Directory: aDirectory
	"this generate the doc for a class in a special directory, generate the doc of the subclass the superclasse and the classe"
	|theStream|
	theStream := (aDirectory / aClass name, 'txt') asFileReference writeStream.
	self writeTitle: 'The Class name : ' 	 withAStream: theStream withContentOnOneLine: aClass name.
	self writeTitle: 'The Superclass Name: ' withAStream: theStream withContentOnOneLine: aClass superclass name.
	self writeTitle: ' The Subclass Name:'   withAStream: theStream withContent: aClass subclasses . 
	self writeVariablesOfTheClass: aClass    withAStream: theStream.
	self writeMethodsOfTheClass: aClass 		 withAStream: theStream.
	theStream close
