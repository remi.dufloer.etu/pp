removing
deleteTheLast
	"you delete a cell on the end of the list, i send you a message with me"
	|celuleBefore |
	celuleBefore := self getByIndex: tail -1.
	celuleBefore next: nil.
	self tail: tail -1 .
	^self.
	