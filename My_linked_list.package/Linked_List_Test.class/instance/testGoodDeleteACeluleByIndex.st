tests
testGoodDeleteACeluleByIndex

	|lastHead |
	lastHead := list head.
	list add: 17 .
	list add: 22 .
	list deleteByIndex: 1.
	self assert: (list head next ) equals: lastHead .
	self assert: list head value equals: 22 .