tests
testgoodAdd

	|lastHead |
	lastHead := list head.
	list add: 17 .
	self assert: list head value equals: 17 .
	self assert: list head next equals: lastHead .
	self assert: list tail equals: 1.
