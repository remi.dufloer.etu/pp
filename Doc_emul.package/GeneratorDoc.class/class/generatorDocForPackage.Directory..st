writing
generatorDocForPackage: anRPackage Directory: aDirectory
	"this generate the doc for all class in a package on a special directory, generate the doc of the subclass the superclasse and the classe"
   (aDirectory / anRPackage name asString) ensureCreateDirectory.
	anRPackage definedClasses do: [ :each | self generatorDocForClass: each Directory: (aDirectory / anRPackage name)].

