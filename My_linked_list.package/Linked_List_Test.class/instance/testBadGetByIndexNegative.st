tests
testBadGetByIndexNegative

	|lastHead |
	lastHead := list head.
	list add: 17 .
	self should: [list getByIndex: (-20)] raise: Error .

