writing
writeVariablesOfTheClass: aClass withAStream: aStream
	"this generate the doc of the variable of the class, the variable class and the instances variable was write on a stream you choose"
	self writeTitle: 'The variables of this classe:' withAStream: aStream withContent: aClass classVariables.
	self writeTitle: 'the instances variables of this classe:' withAStream: aStream withContent: aClass instanceVariables
