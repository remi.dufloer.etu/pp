tests
testGoodDeleteTheLast

	|lastHead |
	lastHead := list head.
	list add: 17 .
	list add: 22 .
	self assert: (list head next next) equals: lastHead .
	list deleteTheLast .
	self assert: (list head next next ) isNil.
	self assert: list head value equals: 22 .
	self assert: list tail equals: 1.
	