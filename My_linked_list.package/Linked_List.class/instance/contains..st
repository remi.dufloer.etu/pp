comparing
contains: 	anObject 
	"send a message with true if the linked list contains the object do you want. if not you send a message with false"
	^ self head contains: anObject .