adding
addAtTheEnd: anObject 
	"you add a new cell on the end of the list, you just give an object and i create the cell, i send you a message with the new cell"
|  thelastCelule |
	thelastCelule := self getByIndex: self tail.
	thelastCelule   next: (Celule  withValue: anObject  withNext: nil) .
	self tail: self tail +1 .
	^ self . 