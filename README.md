# Projet pour le stage 

# Sommaire README
1. [Membre](#membre)
2. [Sujet](#sujet)
3. [Contenue du dépôt](#contenue-du-dépôt)
4. [Explication](#explication)
    1. [Génération de la documentation](#génération-de-la-documentation)
    2. [La linked liste](#la-linked-liste)
5. [Présentation de conception](#présentation-de-conception)


# Membre

- Rémi Dufloer

# Sujet

coder une liste chaînée en Pharo avec des tests. 
- coder une « javadoc » qui prend une classe ou un package en paramètre et qui génère pour chaque classe un fichier avec le nom de la classe, sa super classe, ses sous classes, ses variables d’instance, ses méthodes.

# Contenue du dépôt

    Ce dépôt contient:
                    -  un dossier avec le package gérant la génération de la doc Pharo avec une classe `GeneratorDoc`
                    -  un dossier avec le package gérant la linked list avec les classes `Linked_List`,`Linked_List_Test`,`Celule` `Celule_Test`
                    -  un README qui vous explique comment j'ai procédé pour le dévellopement de ce projet  

# Explication


## Génération de la documentation

La Génération de la documentation ce compose d'une seul classe même si elle aurait pu se composer de deux classe.

J'ai décidé de mettre la generation de documentation de package et de classe ensemble car c'est exactement la même chose 
pour les package on itére juste la generation de documentation sur chaque classe avec la commande `each` .

Ensuite pour la génération de classe il suffit de prendre le nom de la classe et tout d'abord ecrire le nom de la classe puis ecrire sa doc avec pour commencer son nom avec ce quel contient.

Mais la classe peut avoir des sous classe et des hyper classe donc on répéte ces opération pour ces classe  la .

Une classe à des méthode et des variables de classe ou d'instance ainsi il nous faut les écrire.

Mais pour écrire il faut bien un endroit et cette endroit est determiner par un stream dans pharo.

Ensuite pour obtenir toute les informations sur les classe, les classe sont des objets ainsi on peut voir le corps de comment est créée une classe ainsi on obtient énormément d'info via la commande shift entrer.

Pour génerer la documentation d'une classe par exemple il vous suffira d'allez dans le playground et de taper par exemple

`GeneratorDoc generateDocForClass: GeneratorDoc class`

vous trouverez votre javadoc au format txt dans le dossier Home 

## La linked liste

La linked liste ce compose de 2 classe la linked liste en elle même et une classe Celule.

J'ai choisi cette conception car pharo est un langage objet ainsi je me suis mis a m'imaginer prendre une boite avec des cube coupé en deux 
la premiere partie étant la valeur du cube et la deuxieme comment arriver au prochain cube.
Ainsi je me suis rendu compte que je pouvais distingué 2 objet réel la boite avec les cubes et les cubes.

Pour les celule j'ai directement découpé la celule en 2 avec ainsi les valeurs qui ne sont pas forcément un entier mais par exemple un mot 
donc je me suis dit que la valeur serait un objet et pour passez a la prochaine je vais mettre dans la deuxième partie de ce cube directement la nouvelle cellule un peu comme un jeu de domino .

Je ne savais pas vraiment ou m'arreter dans la manipulation de la liste chainé donc j'ai juste repris les fonctions un peu basique
du style recherche, recuperation par index, ajout en tête ajout par index et en fin et pour finir supression en tête par index et en fin.

Je me suis aussi dis que un objet c'est quelque chose qui a de la matière dès la création donc quand on crée une linked liste on est obligé de lui donné une tête pour lui donner de la matière. 

En ayant des blocs je me suis dit qu'avoir la taille de la liste peut être intéressant donc a la création d'une linked liste on commence avec une tête est une boite avec une boite de taille 0. La taille de 0 étant par l'habitude des liste java ou on attrape le premier élement a l'indice 0, même si maintenant je pense que mon choix est très discutable surtout du point de vue objet .


## Présentation de conception

Pour la linked liste j'ai utilisé que du test driven development qui ma montré à quel point c'est satisfaisant contrairement au java, avec les propositions de pharo de créé directement la méthode qu'il manque et de completer petit à petit la classe et de vraiment voir l'objet évoluer.
Le point le plus compliqué au début était juste comprendre le langage et ça syntax ensuite cela allez tout seul.

Pour la génération de doc les choses étaient bien plus compliqué même si toute les méthode existe déja j'ai eu du mal à m'imaginer la chose.
Cependant la commande shift entrer ma était d'une grande aide en pouvant navigué et remonter petit à petit. Je trouve que c'était la partie la plus intéressante de cette exercice "fouiller" dans le langage. 



