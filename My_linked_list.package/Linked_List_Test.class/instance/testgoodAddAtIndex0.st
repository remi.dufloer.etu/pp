tests
testgoodAddAtIndex0

	|lastHead |
	lastHead := list head.
	list addAt: 17 index: 0 .
	self assert: list head value equals: 17 .
	self assert: list head next equals: lastHead .
	self assert: list tail equals: 1.
