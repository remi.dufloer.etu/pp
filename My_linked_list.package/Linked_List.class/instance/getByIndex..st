accessing
getByIndex: anInteger 
	"if you give me a good index, not negative or not superior of the tail, i send you the cell at this index"
	anInteger > self tail ifTrue: [ ^ Error new signal: 'you want a cell not in the list!' ].
	anInteger < 0 ifTrue: [ ^ Error new signal: 'search on positif not on negatif number!' ].
	^ self head getByIndex: anInteger  