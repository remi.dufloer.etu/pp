tests
testGoodPop

	|lastHead |
	lastHead := list head.
	list add: 17 .
	list add: 22 .
	self assert: list tail equals: 2.
	list pop.
	self assert: list head next equals: lastHead .
	self assert: list head value equals: 17 .
	self assert: list tail equals: 1.