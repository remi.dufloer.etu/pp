writing
writeTitle: aString withAStream: aStream withContent: aCollection
	"this write for all content  on a stream with on first part the title and after the content, cr it's for the return at the line "
	aStream nextPutAll: aString; cr.
	aCollection do: [ :each | aStream nextPutAll: ('' join: { ' - '. each asString }  ); cr].
	aStream cr.
