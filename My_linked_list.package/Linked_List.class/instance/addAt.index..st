accessing
addAt: anObject index: anInteger
	"you add a new cell at a index of the list, you just give an object and a index, and i create the cell, i send you a message with the new cell but if you give me a bad index, negative index or index superior of the tail +1 i send you a message with ERROR"
	| celuleBefore newCelule celuleAfter |
	anInteger = 0 ifTrue: [ ^self add: anObject   ].
	anInteger < 0 ifTrue: [ ^ Error new signal: 'you add by a bad index' ].
	anInteger > (tail + 1) ifTrue: [ ^ Error new signal: 'you add after the tail +1 of this list it s imposible' ].
	anInteger = (tail + 1) ifTrue: [ ^ self addAtTheEnd: anObject  ].
	
	celuleBefore := self getByIndex: anInteger -1.
	celuleAfter := self getByIndex: anInteger .
	newCelule := Celule withValue: anObject  withNext: celuleAfter .
	celuleBefore next: newCelule .
	self tail: self tail  + 1 . 
	^ self .