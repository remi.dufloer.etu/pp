tests
testBadDeleteACeluleByIndex

	|lastHead |
	lastHead := list head.
	list add: 17 .
	list add: 22 .
	self should: [list deleteByIndex: 3.] raise: Error .
	self shouldnt:  [list deleteByIndex: 1.] raise: Error  .