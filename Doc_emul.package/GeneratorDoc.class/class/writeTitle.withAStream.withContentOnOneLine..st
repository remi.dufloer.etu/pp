writing
writeTitle: aTitle withAStream: aStream withContentOnOneLine: content
	"this write on a stream on one line the title and the content"
	aStream nextPutAll: ('' join: {aTitle. content}); cr; cr