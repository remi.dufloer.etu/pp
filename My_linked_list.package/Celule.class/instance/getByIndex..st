accessing
getByIndex: anInteger 
	"send a message with the cell at the index do you want"
	anInteger = 0 ifTrue: [ ^self ].
	^ self next getByIndex: anInteger -1
	