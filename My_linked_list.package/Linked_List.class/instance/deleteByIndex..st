removing
deleteByIndex: anInteger 
		"you dell a cell at a index of the list, you just the index, and i delete the cell, i send you a message with the linked list but if you give me a bad index, negative index or index superior of the tail i send you a message with ERROR"
	|celuleBefore celuleAfter|
	anInteger = 0 ifTrue: [ ^ self pop ].
	anInteger > tail ifTrue: [ ^ Error new signal: 'you delete nothing' ].
	anInteger = tail ifTrue: [ ^ self deleteTheLast ].
	celuleBefore := self getByIndex: anInteger - 1.
	celuleAfter  := self getByIndex: anInteger + 1.
	celuleBefore next: celuleAfter.
	self tail: tail -1 .
	^self.