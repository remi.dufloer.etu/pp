tests
testAddATTheEnd

	|lastHead |
	lastHead := list head.
	list add: 17.
	list addAt: 7 index: 2 .
	self assert: list tail equals: 2.
	self assert: list head next next value  equals: 7 .
	self assert: (list head next next next) isNil.