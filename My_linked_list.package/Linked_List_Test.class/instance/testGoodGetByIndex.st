tests
testGoodGetByIndex

	|lastHead |
	lastHead := list head.
	list add: 17 .
	self assert: (list getByIndex: 0) value equals: 17 .
	self assert: (list getByIndex: 0) next equals: lastHead .
	self assert: (list getByIndex: 1) value equals: lastHead value .
	self assert: (list getByIndex: 1) next isNil .

