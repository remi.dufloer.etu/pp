comparing
contains: anObject 
	"send a message with true if the cell have the same value do you want. if not go to next for check but if you dont have next cell you send a message with false"
	self value = anObject ifTrue: [ ^ true  ] .
	self next ifNotNil: [ ^ self next contains: anObject  ].
	self next ifNil: [ ^ false ] 