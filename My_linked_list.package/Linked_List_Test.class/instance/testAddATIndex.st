tests
testAddATIndex

	|lastHead |
	lastHead := list head.
	list add: 17.
	list addAt: 7 index: 1 .
	self assert: list head next next equals: lastHead .
	self assert: list head next value equals: 7 .
	self assert: list tail equals: 2.