tests
testGoodGetByIndex 
|c|
c := Celule withValue: 16  withNext: (Celule withValue: 14 withNext:( Celule withValue: 12 withNext: nil)) .
self assert: (c getByIndex:2) value equals: 12.