tests
testBadAddAtIndexSupOfTheTailPlus1

	|lastHead |
	lastHead := list head.
	list add: 17 .
	list add: 22 .
	self should: [list addAt: 7 index: (4).] raise: Error .
	self shouldnt:  [list addAt: 7 index:  3.] raise: Error  .