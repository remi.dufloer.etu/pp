tests
testBadGetByIndexSupOfTheTail

	|lastHead |
	lastHead := list head.
	list add: 17 .
	self should: [list getByIndex: (list tail +1)] raise: Error .

