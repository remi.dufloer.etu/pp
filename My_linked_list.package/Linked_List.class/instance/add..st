adding
add: anObject
	"you add a new cell on head of the list, you just give an object and i create the cell, i send you a message with the new cell"
	|c newhead lastTail|
	c := self head .
	newhead := Celule withValue: anObject  withNext: c.
	lastTail := self tail .
	self tail: lastTail + 1 . 
	^ self head: newhead.